# translation of docs_krita_org_reference_manual___brushes___brush_settings.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-21 12:30+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/brushes/brush_settings.rst:5
msgid "Brush Settings"
msgstr "Nastavenia štetca"

#: ../../reference_manual/brushes/brush_settings.rst:7
msgid "Overall Brush Settings for the various brush engines."
msgstr "Všeobecné nastavenia kief pre rôzne kefové nástroje."

#: ../../reference_manual/brushes/brush_settings.rst:9
msgid "Contents:"
msgstr "Obsah:"
