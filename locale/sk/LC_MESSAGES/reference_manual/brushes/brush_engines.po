# translation of docs_krita_org_reference_manual___brushes___brush_engines.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-21 12:29+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/brushes/brush_engines.rst:5
msgid "Brush Engines"
msgstr "Kefové nástroje"

#: ../../reference_manual/brushes/brush_engines.rst:7
msgid ""
"Information on the brush engines that can be accessed in the brush editor."
msgstr "Informácia o kefových nástrojoch sa dá získať cez editor kief."

#: ../../reference_manual/brushes/brush_engines.rst:9
msgid "Available Engines:"
msgstr "Dostupné nástroje:"
