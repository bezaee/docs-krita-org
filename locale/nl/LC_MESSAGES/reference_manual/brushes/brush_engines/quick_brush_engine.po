# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-30 16:16+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:1
msgid "The Quick Brush Engine manual page."
msgstr "De handleidingpagina Snelpenseel-engine."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:15
msgid "Quick Brush Engine"
msgstr "Snelpenseel-engine"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:10
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:19
msgid ".. image:: images/icons/quickbrush.svg"
msgstr ".. image:: images/icons/quickbrush.svg"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:20
msgid ""
"A Brush Engine inspired by the common artist's workflow where a simple big "
"brush, like a marker, is used to fill large areas quickly, the Quick Brush "
"engine is an extremely simple, but quick brush, which can give the best "
"performance of all Brush Engines."
msgstr ""
"Een Snelpenseel-engine geïnspireerd door de algemene werkwijze van een "
"artiest waar een eenvoudige grote kwast, zoals een marker, gebruikt wordt om "
"grote vlakken snel te vullen, de Snelpenseel-engine is een extreem "
"eenvoudige, maar snel penseel, die de beste prestaties geeft van alle "
"penseel-engines."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:22
msgid ""
"It can only change size, blending mode and spacing, and this allows for "
"making big optimisations that aren't possible with other brush engines."
msgstr ""
"Het kan alleen de grootte, mengmodus en afstand wijzigen en dit biedt grote "
"optimalisaties die niet mogelijk zijn met andere penseel-engines."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:26
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:27
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:30
msgid "Brush"
msgstr "Penseel"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:32
msgid "The only parameter specific to this brush."
msgstr "Het enige parameter specifiek voor dit penseel."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:34
msgid "Diameter"
msgstr "Diameter"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:35
msgid ""
"The size. This brush engine can only make round dabs, but it can make them "
"really fast despite size."
msgstr ""
"De Grootte. Deze penseel-engine kan alleen ronde stippen maken, maar het kan "
"ze echt zeer snel maken ongeacht de grootte."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid "Spacing"
msgstr "Spatiëring"

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:37
msgid ""
"The spacing between the dabs. This brush engine is particular in that it's "
"faster with a lower spacing, unlike all other brush engines."
msgstr ""
"De afstand tussen de stippen. Deze penseel-engine is speciaal omdat het "
"sneller is met een lagere afstand, anders dan alle andere penseel-engines."

#: ../../reference_manual/brushes/brush_engines/quick_brush_engine.rst:40
msgid "`Phabricator Task <https://phabricator.kde.org/T3492>`_"
msgstr "`Phabricator Task <https://phabricator.kde.org/T3492>`_"
