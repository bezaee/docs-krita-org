# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:07+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats/file_png.rst:1
msgid "The Portable Network Graphics file format in Krita."
msgstr "Het bestandsformaat Portable Network Graphics in Krita."

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "*.png"
msgstr "*.png"

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "png"
msgstr "png"

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "portable network graphics"
msgstr "portable network graphics"

#: ../../general_concepts/file_formats/file_png.rst:17
msgid "\\*.png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:19
msgid ""
"``.png``, or Portable Network Graphics, is a modern alternative to :ref:"
"`file_gif` and with that and :ref:`file_jpg` it makes up the three main "
"formats that are widely supported on the internet."
msgstr ""
"``.png``, of Portable Network Graphics, is een modern alternatief voor :ref:"
"`file_gif` en daarmee en :ref:`file_jpg` vormt het de drie hoofdformaten die "
"breed ondersteund worden op het internet."

#: ../../general_concepts/file_formats/file_png.rst:21
msgid ""
"PNG is a :ref:`lossless <lossless_compression>` file format, which means "
"that it is able to maintain all the colors of your image perfectly. It does "
"so at the cost of the file size being big, and therefore it is recommended "
"to try :ref:`file_jpg` for images with a lot of gradients and different "
"colors. Grayscale images will do better in PNG as well as images with a lot "
"of text and sharp contrasts, like comics."
msgstr ""
"PNG is een :ref:`verliesloos <lossless_compression>` bestandsformaat, wat "
"betekent dat het in staat is alle kleuren in uw afbeelding perfect te "
"behouden. Het doet dat ten koste van de bestandsgrootte die groot is en het "
"is daarom aanbevolen om :ref:`file_jpg` te proberen voor afbeeldingen met "
"veel kleurverloop en verschillende kleuren. Grijze afbeeldingen zullen het "
"beter doen in PNG evenals afbeeldingen met veel tekst en scherpe contrasten, "
"zoals strips."

#: ../../general_concepts/file_formats/file_png.rst:23
msgid ""
"Like :ref:`file_gif`, PNG can support indexed color. Unlike :ref:`file_gif`, "
"PNG doesn't support animation. There have been two attempts at giving "
"animation support to PNG, APNG and MNG, the former is unofficial and the "
"latter too complicated, so neither have really taken off yet."
msgstr ""
"Evenals :ref:`file_gif` kan PNG geïndexeerde kleuren ondersteunen. Anders "
"dan :ref:`file_gif` ondersteunt PNG geen animatie. Er zijn twee pogingen "
"geweest om ondersteuning aan PNG te geven, APNG en MNG, de eerste is niet "
"officieel en de tweede te gecompliceerd, dus geen ervan is echt van de grond "
"gekomen."

#: ../../general_concepts/file_formats/file_png.rst:25
msgid ""
"Since 4.2 we support saving HDR to PNG as according to the `W3C PQ HDR PNG "
"standard <https://www.w3.org/TR/png-hdr-pq/>`_. To save as such files, "
"toggle :guilabel:`Save as HDR image (Rec. 2020 PQ)`, which will convert your "
"image to the Rec 2020 PQ color space and then save it as a special HDR PNG."
msgstr ""
"Sinds 4.2 ondersteunen we opslaan van HDR naar PNG volgens de `W3C PQ HDR "
"PNG standaard <https://www.w3.org/TR/png-hdr-pq/>`_. Om zulke bestanden op "
"te slaan schakel om :guilabel:`Als HDR afbeelding opslaan (Rec. 2020 PQ)`, "
"waarmee uw afbeelding geconverteerd wordt naar de Rec 2020 PQ kleurruimte en "
"dan wordt opgeslagen als een speciale HDR-PNG."
