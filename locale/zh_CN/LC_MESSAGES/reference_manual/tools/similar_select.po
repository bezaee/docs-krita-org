msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___similar_select.pot\n"

#: ../../<generated>:1
msgid "Fuzziness"
msgstr "模糊度"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"

#: ../../reference_manual/tools/similar_select.rst:1
msgid "Krita's similar color selection tool reference."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Selection"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Similar Selection"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:16
msgid "Similar Color Selection Tool"
msgstr "相似颜色选区工具"

#: ../../reference_manual/tools/similar_select.rst:18
msgid "|toolselectsimilar|"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:20
msgid ""
"This tool, represented by a dropper over an area with a dashed border, "
"allows you to make :ref:`selections_basics` by selecting a point of color. "
"It will select any areas of a similar color to the one you selected. You can "
"adjust the \"fuzziness\" of the tool in the tool options dock. A lower "
"number will select colors closer to the color that you chose in the first "
"place."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:23
msgid "Hotkeys and Sticky keys"
msgstr "快捷键和粘滞键"

#: ../../reference_manual/tools/similar_select.rst:25
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:26
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:27
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:28
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:29
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:30
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:31
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:35
msgid "Hovering over a selection allows you to move it."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:36
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:40
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:43
msgid "Tool Options"
msgstr "工具选项"

#: ../../reference_manual/tools/similar_select.rst:46
msgid ""
"This controls whether or not the contiguous selection sees another color as "
"a border."
msgstr ""
