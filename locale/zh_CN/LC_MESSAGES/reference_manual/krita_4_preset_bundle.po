msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___krita_4_preset_bundle.pot\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita4_z-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:1
msgid "Overview of the Krita 4.0 preset bundle."
msgstr "介绍 Krita 的自带笔刷预设。"

#: ../../reference_manual/krita_4_preset_bundle.rst:12
msgid "Resources"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:17
msgid "Krita 4 Preset Bundle Overview"
msgstr "Krita 4 自带笔刷预设介绍"

#: ../../reference_manual/krita_4_preset_bundle.rst:20
msgid ".. image:: images/brushes/Krita4_0_brushes.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:21
msgid ""
"Krita comes with a large collection of brush presets. This collection was "
"designed with many considerations:"
msgstr ""
"Krita 自带了一套种类丰富的笔刷预设，这套笔刷是按照下面的指导思想设计的："

#: ../../reference_manual/krita_4_preset_bundle.rst:23
msgid ""
"Help the beginner and the advanced user with brushes that are ready-to-use."
msgstr "提供一套无论是对初学者还是高手都足够好用的笔刷。"

#: ../../reference_manual/krita_4_preset_bundle.rst:24
msgid ""
"Propose tools for the various ways Krita is used: Comic inking and coloring, "
"Digital Painting, Mate Painting, Pixel Art, 3D texturing."
msgstr ""
"为 Krita 的典型用途准备相应的工具，包括：漫画勾线和上色、数字绘画、影视绘景、"
"像素画、3D 纹理等。"

#: ../../reference_manual/krita_4_preset_bundle.rst:25
msgid "Show a sample of what the brush engines can do."
msgstr "示范各种笔刷引擎的功能，为用户日后定制笔刷提供参考。"

#: ../../reference_manual/krita_4_preset_bundle.rst:27
msgid ""
"This page illustrates and describes the included default brush presets in "
"Krita 4."
msgstr ""
"此页面将介绍 Krita 4 的全套自带笔刷预设，并附带它们的实际效果图。带有旋转角标"
"的笔刷支持笔身倾斜传感器。带有水滴角标的笔刷与画布上的已有颜色会发生混色。各"
"个笔刷的先后顺序与在笔刷预设面板中的一致，并按家族分类。由于当前软件资源管理"
"系统的限制，自带预设的名称无法被翻译。作为权宜之计，可先使用本页面的介绍作为"
"参考。在新资源管理系统就位之前，我们将设法准备中文名称的替代资源包，敬请期"
"待。"

#: ../../reference_manual/krita_4_preset_bundle.rst:30
msgid "Erasers"
msgstr "橡皮擦"

#: ../../reference_manual/krita_4_preset_bundle.rst:32
msgid ""
"The large one is for removing large portions of a layer (eg. a full "
"character)."
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:33
msgid ""
"The small one is designed to use when drawing thin lines or inking. It has a "
"very specific shape so you will notice with the square shape of your cursor "
"you are in eraser-mode."
msgstr "小型橡皮擦有一个方形的笔尖，边缘锐利，用于绘制细小区域和勾线时使用。"

#: ../../reference_manual/krita_4_preset_bundle.rst:34
msgid ""
"The soft one is used to erase or fade out the part of a drawing with various "
"levels of opacity."
msgstr "软质橡皮擦的擦除效果带有明显的淡化和透明度。"

#: ../../reference_manual/krita_4_preset_bundle.rst:37
msgid ".. image:: images/brushes/Krita4_a-brush-family.png"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:39
msgid "Basics"
msgstr "基本笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:41
msgid ""
"The basic brush family all use a basic circle for the brush tip with a "
"variation on opacity, flow or size. They are named Basic because brushes of "
"this type are the fundamental stones of every digital painting program. "
"These brushes will work fast since they use simple properties."
msgstr ""
"基本笔刷家族全部使用了圆形的笔尖，但各自使用了不同不透明度、流量和大小选项。"
"任何一款数字绘画软件都会自带类似的笔刷。由于这些笔刷的属性相对简单，它们的处"
"理速度也相对较快。"

#: ../../reference_manual/krita_4_preset_bundle.rst:44
msgid ".. image:: images/brushes/Krita4_b-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:46
msgid "Pencils"
msgstr "铅笔"

#: ../../reference_manual/krita_4_preset_bundle.rst:48
msgid ""
"These presets tends to emulate the effect of pencil on paper. They all have "
"a thin brush that uses a paper-texture. Some focus on being realistic to "
"help with correcting a pencil scan. Some focus more on showing the effects "
"on your computer monitor. The two last (Tilted/Quick Shade) assist the "
"artist to obtain specific effects; like quickly shading a large area of the "
"drawing without having to manually crosshatch a lot of lines."
msgstr ""
"这组笔刷模仿了铅笔在纸张上的绘制效果，它们全部带有纸质纹理且颜色较淡。有些适"
"合用来修改扫描的铅笔稿，有些则适合在屏幕上画出效果。铅笔 5 号和铅笔 6 号可以"
"模仿铅笔铺调子时的特有质感。"

#: ../../reference_manual/krita_4_preset_bundle.rst:51
msgid ".. image:: images/brushes/Krita4_c-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:53
msgid "Inking"
msgstr "勾线笔"

#: ../../reference_manual/krita_4_preset_bundle.rst:55
msgid ""
"For the black & white illustrator or the comic artist. The Inking brushes "
"help you produce line art and high contrast illustrations."
msgstr "这组笔刷是为绘制黑白插图和漫画准备的，它们适合绘制线稿和高反差的画作。"

#: ../../reference_manual/krita_4_preset_bundle.rst:57
msgid ""
"Ink Precision: A thin line designed to take notes or draw tiny lines or "
"details."
msgstr "勾线笔 1 号 - 极细：适合用来书写笔记和绘画细线的笔刷，宽度极细且固定。"

#: ../../reference_manual/krita_4_preset_bundle.rst:58
msgid ""
"Ink Fineliner: A preset with a regular width to trace panels, technical "
"details, or buildings."
msgstr ""
"勾线笔 2 号 - 较细：用于给漫画边框描边、技术图纸勾线等用途的等宽细头笔刷。"

#: ../../reference_manual/krita_4_preset_bundle.rst:59
msgid "Ink GPen: A preset with a dynamic on size to ink smoothly."
msgstr ""
"勾线笔 3 号 - GPen：笔尖具有一定大小范围，可以描绘出具有平滑粗细变化的线条。"

#: ../../reference_manual/krita_4_preset_bundle.rst:60
msgid ""
"Ink Pen Rough: A preset for inking with a focus on having a realistic ink "
"line with irregularities (texture of the paper, fiber of paper absorption)."
msgstr ""
"勾线笔 4 号 - 写实钢笔：模仿真实画材的勾线笔，带有轻微的纸质纹理和渗墨效果。"

#: ../../reference_manual/krita_4_preset_bundle.rst:61
msgid ""
"Ink Brush Rough: A brush for inking with also a focus on getting the "
"delicate paper texture appearing at low pressure, as if the brush slightly "
"touch paper."
msgstr ""
"勾线笔 7 号 - 写实笔刷：笔刷大小具有较宽范围，在压力较轻时可以绘制出明显的纸"
"质纹理，好像笔刷轻触纸张的效果一样。"

#: ../../reference_manual/krita_4_preset_bundle.rst:62
msgid ""
"Ink Sumi-e: A brush with abilities at revealing the thin texture of each "
"bristle, making the line highly expressive."
msgstr "勾线笔 8 号 - 水墨：能够表现出笔刷鬃毛的纹理，表现力较强。"

#: ../../reference_manual/krita_4_preset_bundle.rst:65
msgid ".. image:: images/brushes/Krita4_d-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:67
msgid "Markers"
msgstr "马克笔"

#: ../../reference_manual/krita_4_preset_bundle.rst:69
msgid ""
"A small category with presets simulating a marker with a slight digital "
"feeling to them."
msgstr "一组模拟马克笔效果的笔刷，它们的效果带有一种微妙的数字笔刷的感觉。"

#: ../../reference_manual/krita_4_preset_bundle.rst:72
msgid ".. image:: images/brushes/Krita4_e-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:74
msgid "Dry Painting"
msgstr "干性笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:76
msgid ""
"The Dry Painting category is full set of brushes that appear like bristles. "
"They do not interact with the color already on the canvas; that's why they "
"are called \"dry\". They work as if you were painting on a dry artwork: the "
"color replace, or overlay/glaze over the previous painting stroke. This "
"brush emulates techniques that dry quickly as tempera or acrylics."
msgstr ""
"这是一组具有鬃毛效果的笔刷，它们不会跟画布上的颜色混在一起发生钝化，所以叫"
"做“干性”笔刷。你可以用它们模仿不透明颜料堆积在画布上覆盖已有颜色的画法，如蛋"
"彩和丙烯颜料等。"

#: ../../reference_manual/krita_4_preset_bundle.rst:79
msgid ".. image:: images/brushes/Krita4_f-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:81
msgid "Dry Painting Textured"
msgstr "干性纹理笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:83
msgid ""
"Almost the same family as the previous one, except these brush presets lay "
"down a textured effect. They simulate the painting effect you can obtain "
"with very thick painting on a brush caressing a canvas with fabric texture. "
"This helps to build painterly background or add life in the last bright "
"touch of colors."
msgstr ""
"这组笔刷几乎可以被归类到前一组里面，但它们的不同之处在于能够画出明显的纹理，"
"模仿非常厚重的颜料被擦过粗糙画面的效果，有助于得到写实的颜料肌理和干净鲜明的"
"空间混色效果。"

#: ../../reference_manual/krita_4_preset_bundle.rst:86
msgid ".. image:: images/brushes/Krita4_g-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:88
msgid "Chalk, Pastel and Charcoal"
msgstr "粉笔、蜡笔和炭笔"

#: ../../reference_manual/krita_4_preset_bundle.rst:90
msgid ""
"Still part of the dry family. These brushes focus on adding texture to the "
"result. The type of texture you would obtain by using a dry tool such as "
"chalk, charcoal or pastel and rubbing a textured paper."
msgstr ""
"这组笔刷也是干性的，主要用来在颜色上增添肌理，效果类似粉笔、蜡笔、炭笔和手擦"
"粗纹纸张的效果。"

#: ../../reference_manual/krita_4_preset_bundle.rst:93
msgid ".. image:: images/brushes/Krita4_h-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:95
msgid "Wet painting"
msgstr "湿性笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:97
msgid ""
"This family of brushes is wet in a sense they all interact with the color on "
"the canvas. It triggers the feeling of having a wet artwork and mixing color "
"at the same time. The category has variations with bristle effects or simple "
"rounded brushes."
msgstr ""
"这组笔刷会跟画布上的已有颜色发生混色，因此是“湿性”画笔。它们模仿在还没有干透"
"的颜色上面混合颜色的效果，包括简单的圆笔尖和鬃毛笔尖。"

#: ../../reference_manual/krita_4_preset_bundle.rst:100
msgid ".. image:: images/brushes/Krita4_i-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:102
msgid "Watercolors"
msgstr "水彩"

#: ../../reference_manual/krita_4_preset_bundle.rst:104
msgid ""
"Simulating real watercolors is highly complex. These brushes only partially "
"simulate the watercolor texture. Don't expect crazy pigment diffusion "
"because these brushes are not able to do that. These brushes are good at "
"simulating a fringe caused by the pigments and various effects."
msgstr ""
"这组笔刷能够分别模拟水彩的某些特点，有的笔刷可以模拟渗出，有的笔刷可以模拟材"
"质吸水，有的笔刷可以模拟边缘颜料沉积。要注意的是：这些笔刷是用来添加水彩效果"
"的，它们并不是在原理层面模拟真实水彩的复杂吸渗和颜料扩散，所以效果存在限制。"

#: ../../reference_manual/krita_4_preset_bundle.rst:107
msgid ".. image:: images/brushes/Krita4_j-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:109
msgid "Blender"
msgstr "混色笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:111
msgid ""
"These brushes don't paint any colors. They interact with the color you "
"already have on the canvas. Don't expect them to have any effect on a white "
"page. All these presets give a different result with how they smudge or "
"smear. It helps to blend colors, blur details, or add style on a painting. "
"Smearing pixels can help with creating smoke and many other effects."
msgstr ""
"这组笔刷本身不会画出颜色，它们只会对画布上的已有颜色进行涂抹。你可以使用它们"
"来混合颜色、模糊细节、营造烟雾等各种效果。"

#: ../../reference_manual/krita_4_preset_bundle.rst:114
msgid ".. image:: images/brushes/Krita4_k-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:116
msgid "Adjustments"
msgstr "调整笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:118
msgid ""
"This family of airbrushes has variations on the blending modes. Different "
"blending modes will give different results depending on the effect you are "
"trying to achieve."
msgstr ""
"这组笔刷用来调整笔尖下面的画布已有颜色，它们的原理是在一个喷枪笔尖中应用不同"
"的混色模式。从上到下分别是："

#: ../../reference_manual/krita_4_preset_bundle.rst:120
msgid ""
"Color - Can help to re-color or desaturate a part of your artwork. It "
"changes only the hue and saturation, not the value, of the pixels."
msgstr "调整 (颜色)：只调整色相和饱和度，不调整画面形状。"

#: ../../reference_manual/krita_4_preset_bundle.rst:121
msgid "Dodge - Will assist you in creating effects such as neon or fire."
msgstr "调整 (减淡)：营造一般光照的效果，类似 Photoshop 的减淡工具。"

#: ../../reference_manual/krita_4_preset_bundle.rst:122
msgid ""
"Lighten - Brightens only the area with the selected color: a good brush to "
"paint depth of field (sfumato) and fog."
msgstr "调整 (变亮)：用当前颜色照亮笔刷区域，适合营造空气透视感。"

#: ../../reference_manual/krita_4_preset_bundle.rst:123
msgid ""
"Multiply - Darkens all the time. A good brush to create a quick vignette "
"effect around an artwork, or to manage big part in shadow."
msgstr ""
"调整 (相乘)：总是把颜色变暗，适用于制作阴影，类似于 Photoshop 的加深工具。"

#: ../../reference_manual/krita_4_preset_bundle.rst:124
msgid ""
"Overlay - Burn helps to boost the contrast and overlay color on some areas."
msgstr ""
"调整 (叠加)：让亮的颜色更亮，暗的颜色更暗，增加颜色反差，也可以叠加颜色。"

#: ../../reference_manual/krita_4_preset_bundle.rst:127
msgid ".. image:: images/brushes/Krita4_l-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:129
msgid "Shapes"
msgstr "形状"

#: ../../reference_manual/krita_4_preset_bundle.rst:131
msgid ""
"Painting with ready-made shapes can help concept artists create happy-"
"accidents and stimulate the imagination. The Shape Fill tool is a bit "
"specific: you can draw a silhouette of shape and Krita fills it in real "
"time. Painting shapes over an area helps fill it with random details. This "
"is useful before painting over with more specific objects."
msgstr ""
"这组笔刷可以绘制出随机形状，方便在进行概念设计时探索可能性，填充随机细节等。"
"你也可以替换它们的笔尖预制形状使用。"

#: ../../reference_manual/krita_4_preset_bundle.rst:134
msgid ".. image:: images/brushes/Krita4_t-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:136
msgid "Pixel"
msgstr "像素画"

#: ../../reference_manual/krita_4_preset_bundle.rst:138
msgid ""
"You might believe this section is specific to pixel-artist, but in many "
"situations dealing with specific pixels are needed to make corrections and "
"adjustments even on normal paintings. A thin 1px brush can be used to trace "
"guidelines. A brush with aliasing is also perfect to fix the color island "
"created by the Coloring-mask feature."
msgstr ""
"这组笔刷可以绘制出精确像素大小的纯色，它们不但适合绘制像素画，对于需要精确控"
"制某几个像素颜色进行调整修复的场合也同样适用，如填补着色蒙版边缘的空像素等。"
"1px 的像素画笔刷也适合用来绘制导线。"

#: ../../reference_manual/krita_4_preset_bundle.rst:141
msgid ".. image:: images/brushes/Krita4_u-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:143
msgid "Experimental"
msgstr "实验笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:145
msgid ""
"When categorizing brushes, there is always a special or miscellaneous "
"category. In this family of brushes you'll find the clone brush along with "
"brushes to move, grow, or shrink specific areas."
msgstr ""
"这组笔刷包括了不便分类的“其他”笔刷。包括克隆、移动、扩大、收缩等特效笔刷和实"
"验性的特殊笔刷都被归到此类。"

#: ../../reference_manual/krita_4_preset_bundle.rst:148
msgid ".. image:: images/brushes/Krita4_v-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:150
msgid "Normal Map"
msgstr "法线贴图笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:152
msgid ""
"Useful for 3D programs and texture artists. If your tablet supports tilting "
"and rotation this brush will allow you to paint on your normal map using "
"your brush rotation and orientation. You can \"sculpt\" your details in the "
"texture with the different colors. Each color will map to an angle that is "
"used for 3D lighting. It works well on pen-tablet display (tablet with a "
"screen) as you can better sync the rotation and tilting of your stylus with "
"the part of the normal map you want to paint."
msgstr ""
"这是用来帮助纹理画手准备 3D 程序所需法线贴图的笔刷。只要数位板支持倾斜、旋转"
"等传感器，这种笔刷就可以让你在纹理上用不同的颜色“雕刻”出立体细节的法线贴图，"
"每一种颜色在 3D 软件中都代表了一个特定的角度。它在配合数位屏使用时更为顺手，"
"因为旋转和倾斜与画面的关系更为直观。"

#: ../../reference_manual/krita_4_preset_bundle.rst:155
msgid ".. image:: images/brushes/Krita4_w-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:157
msgid "Filters"
msgstr "滤镜笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:159
msgid ""
"Krita can apply many of its filters on a brush thanks to the filter brush "
"engine. The result is usually not efficient and slow, but a good demo of the "
"ability of Krita."
msgstr ""
"Krita 可以把许多内置滤镜通过滤镜笔刷引擎应用到笔刷上，虽然处理速度一般都不会"
"特别快。"

#: ../../reference_manual/krita_4_preset_bundle.rst:162
msgid ".. image:: images/brushes/Krita4_x-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:164
msgid "Textures"
msgstr "纹理笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:166
msgid ""
"Adding textures is not only useful for the 3D artist or video-game artist: "
"in many artworks you'll save a lot of time by using brushes with random "
"patterns."
msgstr "这组笔刷可以快速地绘制一些特定模式的复杂纹理，提高作业效率。"

#: ../../reference_manual/krita_4_preset_bundle.rst:169
msgid ".. image:: images/brushes/Krita4_y-brush-family.jpg"
msgstr ""

#: ../../reference_manual/krita_4_preset_bundle.rst:171
msgid "Stamps"
msgstr "印章笔刷"

#: ../../reference_manual/krita_4_preset_bundle.rst:173
msgid ""
"The stamps are a bit similar to the texture category. Stamps often paint a "
"pattern that is easier to recognize than if you tried to paint it manually. "
"The results appear more as decorations than for normal painting methods."
msgstr ""
"这组笔刷和纹理近似，专门用来绘制某些装饰性的大致图案，而无需手动把它们一笔一"
"笔地画出来。"
