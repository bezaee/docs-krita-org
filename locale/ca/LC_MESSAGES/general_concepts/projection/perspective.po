# Translation of docs_krita_org_general_concepts___projection___perspective.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-24 14:52+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../general_concepts/projection/perspective.rst:None
msgid ""
".. image:: images/category_projection/Projection_Lens1_from_wikipedia.svg"
msgstr ""
".. image:: images/category_projection/Projection_Lens1_from_wikipedia.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection-cube_12.svg"
msgstr ".. image:: images/category_projection/projection-cube_12.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection-cube_13.svg"
msgstr ".. image:: images/category_projection/projection-cube_13.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_31.png"
msgstr ".. image:: images/category_projection/projection_image_31.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_animation_03.gif"
msgstr ".. image:: images/category_projection/projection_animation_03.gif"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_32.png"
msgstr ".. image:: images/category_projection/projection_image_32.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_33.png"
msgstr ".. image:: images/category_projection/projection_image_33.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_34.png"
msgstr ".. image:: images/category_projection/projection_image_34.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_35.png"
msgstr ".. image:: images/category_projection/projection_image_35.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_36.png"
msgstr ".. image:: images/category_projection/projection_image_36.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_37.png"
msgstr ".. image:: images/category_projection/projection_image_37.png"

# skip-rule: punctuation-period
#: ../../general_concepts/projection/perspective.rst:1
msgid "Perspective projection."
msgstr "Projecció en perspectiva"

#: ../../general_concepts/projection/perspective.rst:10
msgid ""
"This is a continuation of the :ref:`axonometric tutorial "
"<projection_axonometric>`, be sure to check it out if you get confused!"
msgstr ""
"Aquesta és una continuació de la :ref:`guia d'aprenentatge de la projecció "
"axonomètrica <projection_axonometric>`, assegureu-vos de comprovar-la si us "
"confoneu!"

#: ../../general_concepts/projection/perspective.rst:12
#: ../../general_concepts/projection/perspective.rst:16
msgid "Perspective Projection"
msgstr "Projecció en perspectiva"

#: ../../general_concepts/projection/perspective.rst:12
msgid "Projection"
msgstr "Projecció"

#: ../../general_concepts/projection/perspective.rst:12
msgid "Perspective"
msgstr "Perspectiva"

#: ../../general_concepts/projection/perspective.rst:18
msgid ""
"So, up till now we’ve done only parallel projection. This is called like "
"that because all the projection lines we drew were parallel ones."
msgstr ""
"Així doncs, fins ara només hem fet projecció en paral·lel. S'anomena així "
"perquè totes les línies de la projecció que hem dibuixat estan paral·leles."

#: ../../general_concepts/projection/perspective.rst:20
msgid ""
"However, in real life we don’t have parallel projection. This is due to the "
"lens in our eyes."
msgstr ""
"No obstant això, en la vida real no tenim una projecció en paral·lel. Això "
"es deu a la lent en els nostres ulls."

# skip-rule: t-acc_obe
#: ../../general_concepts/projection/perspective.rst:25
msgid ""
"Convex lenses, as this lovely image from `wikipedia <https://en.wikipedia."
"org/wiki/Lens_%28optics%29>`_ shows us, have the ability to turn parallel "
"lightrays into converging ones."
msgstr ""
"Les lents convexes, tal com ens mostra aquesta bella imatge de la "
"`Viquipèdia <https://ca.wikipedia.org/wiki/Lent>`_, tenen la capacitat de "
"convertir els raigs de llum paral·lels en convergents."

#: ../../general_concepts/projection/perspective.rst:27
msgid ""
"The point where all the rays come together is called the focal point, and "
"the vanishing point in a 2d drawing is related to it as it’s the expression "
"of the maximum distortion that can be given to two parallel lines as they’re "
"skewed toward the focal point."
msgstr ""
"El punt on s'uneixen tots els raigs s'anomena punt focal, i el punt de fuga "
"en un dibuix en 2D està relacionat amb aquest, ja que és l'expressió de la "
"distorsió màxima que es pot donar a dues línies paral·leles a mesura que "
"s'inclinen cap al punt focal."

#: ../../general_concepts/projection/perspective.rst:29
msgid ""
"As you can see from the image, the focal point is not an end-point of the "
"rays. Rather, it is where the rays cross before diverging again… The only "
"difference is that the resulting image will be inverted. Even in our eyes "
"this inversion happens, but our brains are used to this awkwardness since "
"childhood and turn it around automatically."
msgstr ""
"Com podeu veure a la imatge, el punt focal no és el punt final dels raigs. "
"Més aviat, és on es creuen els raigs abans de divergir novament... L'única "
"diferència és que la imatge resultant s'invertirà. Aquesta inversió succeeix "
"fins i tot amb els nostres ulls, però el nostre cervell està acostumat a "
"aquest desgavell des de la infància i li donarà la volta de manera "
"automàtica."

#: ../../general_concepts/projection/perspective.rst:31
msgid "Let’s see if we can perspectively project our box now."
msgstr "Vegem si ara podem projectar en perspectiva el nostre quadre."

#: ../../general_concepts/projection/perspective.rst:36
msgid ""
"That went pretty well. As you can see we sort of *merged* the two sides into "
"one (resulting into the purple side square) so we had an easier time "
"projecting. The projection is limited to one or two vanishing point type "
"projection, so only the horizontal lines get distorted. We can also distort "
"the vertical lines"
msgstr ""
"Això va força bé. Com podeu veure, hem de *fusionar* els dos costats en un "
"(el qual resultarà en el costat porpra del quadrat) perquè sigui més fàcil a "
"l'hora de projectar. La projecció està limitada a una o dues projeccions del "
"tipus punt de fuga, de manera que només es distorsionen les línies "
"horitzontals. També podem distorsionar les línies verticals"

#: ../../general_concepts/projection/perspective.rst:41
msgid ""
"… to get three-point projection, but this is a bit much. (And I totally made "
"a mistake in there…)"
msgstr ""
"...per obtenir una projecció de tres punts, però això és una mica massa. (I "
"aquí m'he equivocat totalment...)"

#: ../../general_concepts/projection/perspective.rst:43
msgid "Let’s setup our perspective projection again…"
msgstr "Configurarem la nostra projecció en perspectiva de nou..."

#: ../../general_concepts/projection/perspective.rst:48
msgid ""
"We’ll be using a single vanishing point for our focal point. A guide line "
"will be there for the projection plane, and we’re setting up horizontal and "
"vertical parallel rules to easily draw the straight lines from the view "
"plane to where they intersect."
msgstr ""
"Utilitzarem un únic punt de fuga per al nostre punt focal. Hi haurà una "
"línia guia per al pla de la projecció, i establirem regles paral·lels "
"horitzontals i verticals per a dibuixar amb facilitat les línies rectes des "
"del pla de visió fins al punt d'intersecció."

#: ../../general_concepts/projection/perspective.rst:50
msgid ""
"And now the workflow in GIF format… (don’t forget you can rotate the canvas "
"with the :kbd:`4` and :kbd:`6` keys)"
msgstr ""
"I ara el flux de treball per al format GIF... (recordeu que es pot girar el "
"llenç amb les tecles :kbd:`4` i :kbd:`6`)."

#: ../../general_concepts/projection/perspective.rst:55
msgid "Result:"
msgstr "Resultat:"

#: ../../general_concepts/projection/perspective.rst:60
msgid "Looks pretty haughty, doesn’t he?"
msgstr "Sembla molt arrogant, no?"

#: ../../general_concepts/projection/perspective.rst:62
msgid "And again, there’s technically a simpler setup here…"
msgstr "I de nou, tècnicament hi ha una configuració més senzilla..."

#: ../../general_concepts/projection/perspective.rst:64
msgid "Did you know you can use Krita to rotate in 3d? No?"
msgstr "Sabíeu que podeu utilitzar el Krita per a girar en 3D? No?"

#: ../../general_concepts/projection/perspective.rst:69
msgid "Well, now you do."
msgstr "Bé, ara sí."

#: ../../general_concepts/projection/perspective.rst:71
msgid "The ortho graphics are being set to 45 and 135 degrees respectively."
msgstr ""
"Els gràfics ortogràfics s'han establert a 45 i 135 graus respectivament."

#: ../../general_concepts/projection/perspective.rst:73
msgid ""
"We draw horizontal lines on the originals, so that we can align vanishing "
"point rulers to them."
msgstr ""
"Dibuixem línies horitzontals sobre els originals, de manera que puguem "
"alinear-les amb els regles dels punts de fuga."

#: ../../general_concepts/projection/perspective.rst:78
msgid ""
"And from this, like with the shearing method, we start drawing. (Don’t "
"forget the top-views!)"
msgstr ""
"I a partir d'aquí, com amb el mètode d'inclinar, comencem a dibuixar. (No "
"oblideu les vistes superiors!)"

#: ../../general_concepts/projection/perspective.rst:80
msgid "Which should get you something like this:"
msgstr "El que hauria de donar-vos quelcom com això:"

#: ../../general_concepts/projection/perspective.rst:85
msgid "But again, the regular method is actually a bit easier..."
msgstr "Però novament, el mètode habitual és en realitat una mica més fàcil..."

#: ../../general_concepts/projection/perspective.rst:87
msgid ""
"But now you might be thinking: gee, this is a lot of work… Can’t we make it "
"easier with the computer somehow?"
msgstr ""
"Però ara podríeu estar pensant: Caram! Això és molta feina... No podem "
"facilitar-ho d'alguna manera amb l'ordinador?"

# skip-rule: t-sc-yes
#: ../../general_concepts/projection/perspective.rst:89
msgid ""
"Uhm, yes, that’s more or less why people spent time on developing 3d "
"graphics technology:"
msgstr ""
"Per descomptat, aquí és més o menys on passa el temps la gent que "
"desenvolupa tecnologia de gràfics en 3D:"

#: ../../general_concepts/projection/perspective.rst:97
msgid ""
"(The image above is sculpted in blender using our orthographic reference)"
msgstr ""
"(La imatge de dalt està esculpida en Blender emprant la nostra referència "
"ortogràfica)."

#: ../../general_concepts/projection/perspective.rst:99
msgid ""
"So let us look at what this technique can be practically used for in the "
"next part..."
msgstr ""
"Així que vegem una aplicació pràctica d'aquesta tècnica en la següent part..."
