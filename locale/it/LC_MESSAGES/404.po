# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Valter Mura <valtermura@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-09 23:58+0200\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Immagine di Kiki che cerca confusa tra i libri."

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "File non trovato (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "Questa pagina non esiste."

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "Si potrebbe trattare del problema seguente:"

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""
"Abbiamo spostato il manuale da MediaWiki a Sphinx e l'abbiamo anche "
"profondamente riorganizzato."

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "La pagina è stata rimossa."

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "Oppure è stato un semplice errore di digitazione dell'URL."

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
"In ogni caso, potrai trovare la pagina che cerchi utilizzando l'opzione di "
"ricerca che si trova nell'area sinistra di navigazione."
